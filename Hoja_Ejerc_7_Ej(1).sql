﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 5 tablas
DROP DATABASE IF EXISTS b20190607;
CREATE DATABASE b20190607;

-- Seleccionar la base de datos
USE b20190607;

/*
  creando la tabla 
*/

CREATE TABLE D(
  cod_dpt varchar(10),
  PRIMARY KEY(cod_dpt) -- creando la clave
);

CREATE TABLE E(
  dni varchar(20),
  PRIMARY KEY(dni)
);

CREATE TABLE P(
  cod_dpt varchar(10),
  dni varchar(20),
  PRIMARY KEY(cod_dpt,dni),  -- creando la clave 
  UNIQUE(dni),
  CONSTRAINT fke_p FOREIGN KEY(dni)
  REFERENCES E(dni),
  CONSTRAINT fkd_p FOREIGN KEY(cod_dpt)
  REFERENCES D(cod_dpt)
);


CREATE TABLE Pryct(
cod_pry varchar(10),
  PRIMARY KEY(cod_pry)
);

CREATE TABLE T(
  dni varchar(20),
  cod_pry varchar(10),
  fecha date,
  PRIMARY KEY(dni,cod_pry),  -- creando la clave 
  CONSTRAINT fkemp_trab FOREIGN KEY(dni)
  REFERENCES E(dni),
  CONSTRAINT fktrab_pry FOREIGN KEY(cod_pry)
  REFERENCES Pryct(cod_pry)
);


